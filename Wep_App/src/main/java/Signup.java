import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

@WebServlet("/signup")
public class Signup extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        System.out.println("hello for");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection conn = DataBase.getConnection();

        String first_name = request.getParameter("First_Name");
        String last_name = request.getParameter("Last_Name");
        String email = request.getParameter("Email");
        String pass = request.getParameter("Pass");



        try {
            Statement st = conn.createStatement();

        if (first_name != null || last_name != null ){

            String cookie = Home.setCookie(response);

            insertToDataBase(first_name, last_name, email, pass, cookie, st);
            // home page has two Attribute  First_Name and Last_Name
            request.setAttribute("First_Name", first_name);
            request.setAttribute("Last_Name", last_name);
            request.getRequestDispatcher("/Home.ftlh").forward(request, response);
        }else {
            request.getRequestDispatcher("/NotFoundUser.html").forward(request, response);
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void insertToDataBase(String first_name, String last_name, String email, String pass, String cookie, Statement st) throws SQLException {

        st.execute("insert into users_test (First_Name, Last_Name, Email_Address, Password,cooki_id)" +
                " values ('" + first_name + "','" + last_name + "','" + email + "','" + pass + "','" + cookie + "')");
    }
}
